Welcome to NotITG's (unofficial) documentation!
===============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   versions
   lua_api/index
   message_commands
   modifiers
   acknowledgements

Indices and tables
==================

- :ref:`genindex`
- :ref:`search`

.. - :ref:`modindex`

Contributing
============

Want to help contribute to these docs? The source is available at https://gitlab.com/CraftedCart/notitg_docs
