Acknowledgements
================

These docs are currently maintained by CraftedCart, though it would not be possible without the resources of others.

Thanks to HeySora for the original NotITG and OpenITG documentation, hosted at https://sm.heysora.net/doc/, and
available on GitHub at https://github.com/HeySora/Lua-ITG.

Thanks to Jaezmien for maintaining a fork of the original docs, containing information on some newer (|notitg_v3| /
|notitg_v4|) methods, available at https://github.com/Jaezmien/Lua-ITG.

Thanks for Jose_Valera for hosting the StepMania Builds Archive at https://jose.heysora.net/SMArchive/Builds/index.htm
and keeping around older NotITG versions, amoung other cool things.

Thanks to the project developers, and the people who create mod files with the game, for helping out on the UKSRT
Discord group, as well as for the large library of mod files I can search through for Lua examples.
