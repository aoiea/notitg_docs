Modifiers
=========

|since_itg|

.. contents:: :local:

All mods available as of NotITG 20200511

All # are ints, example:
spline#rotx# can be spline1rotx3

All % are floats, example:
%x can be 1.5x

This list is complete (as of |notitg_v4_0_1|), but sorely lacking in descriptions/images. Want to help contribute to
these docs? The source is available at https://gitlab.com/CraftedCart/notitg_docs

<noteskin>
----------
where ``noteskin`` is the name of a note skin

Sets the player's noteskin (eg: ``metal``)

<speed>x
--------
where ``<speed>`` is a float

|since_itg|

Sets the scroll speed to use XMod at a given speed

AddScore
--------

Alternate
---------

|since_itg|

Reverses the second and forth columns

https://fms-cat.com/flip-invert/

ApproachType
------------

ArrowCull
---------

ArrowPath
---------

Shows lines for the path arrows take to the receptors

ArrowPath#
----------

ArrowPathDiffuse|%|%|%
----------------------

ArrowPathDrawDistance
---------------------

ArrowPathDrawDistanceBack
-------------------------

ArrowPathDrawDistanceFront
--------------------------

ArrowPathDrawSize
-----------------

ArrowPathDrawSizeBack
---------------------

ArrowPathDrawSizeFront
----------------------

ArrowPathFadeBottom|%|%|%
-------------------------

ArrowPathFadeBottomOffset|%|%|%
-------------------------------

ArrowPathFadeTop|%|%|%
----------------------

ArrowPathFadeTopOffset|%|%|%
----------------------------

ArrowPathGirth
--------------

ArrowPathGrain
--------------

ArrowPathGranulate
------------------

ArrowPathSize
-------------

ArrowPathWidth
--------------

Asymptote
---------

AsymptoteOffset
---------------

AsymptoteScale
--------------

AsymptoteSize
-------------

Attenuate
---------

AttenuateOffset
---------------

AttenuateX
----------

AttenuateXOffset
----------------

AttenuateZ
----------

AttenuateZOffset
----------------

AverageScore
------------

Beat
----

|since_itg|

Makes the arrows/receptors shake on beats

BeatCap
-------

BeatCapY
--------

BeatCapz
--------

BeatMult
--------

BeatOffset
----------

BeatPeriod
----------

BeatSize
--------

BeatY
-----

BeatYMult
---------

BeatYOffset
-----------

BeatYPeriod
-----------

BeatYSize
---------

BeatZ
-----

BeatZMult
---------

BeatZOffset
-----------

BeatZPeriod
-----------

BeatZSize
---------

Big
---

Blind
-----

Blink
-----

|since_itg|

Arrows flash between visible and hidden

BlinkB
------

BlinkBlue
---------

BlinkG
------

BlinkGreen
----------

BlinkR
------

BlinkRed
--------

Bmrize
------

Boomerang
---------

Boost
-----

Bounce
------

Bounceoffset
------------

Bounceperiod
------------

Bouncez
-------

Bouncezoffset
-------------

Bouncezperiod
-------------

Brake
-----

Bumpy
-----

|since_itg|

The notes move forward and backwards (along the depth axis)

This also enables depth testing

Bumpy#
------

Bumpyoffset
-----------

Bumpyoffset#
------------

Bumpyperiod
-----------

Bumpyperiod#
------------

Bumpysize
---------

Bumpyx
------

Bumpyx#
-------

Bumpyxoffset
------------

Bumpyxoffset#
-------------

Bumpyxperiod
------------

Bumpyxperiod#
-------------

Bumpyxsize
----------

Bumpyy
------

Bumpyy#
-------

Bumpyyoffset
------------

Bumpyyoffset#
-------------

Bumpyyperiod
------------

Bumpyyperiod#
-------------

Bumpyysize
----------

Bumpyz
------

Bumpyz#
-------

Bumpyzoffset
------------

Bumpyzoffset#
-------------

Bumpyzperiod
------------

Bumpyzperiod#
-------------

Bumpyzsize
----------

C<speed>
--------
where ``<speed>`` is a float

|since_itg|

Sets the scroll speed to use CMod at a given speed

Centered
--------

Centered2
---------

ClearAll
--------

Resets all mods

Also resets splines if :lua:meth:`Player.NoClearSplines` is ``false``

Confusion
---------

The notes and receptors spin

ConfusionOffset
---------------

Offset the rotation for the Confusion mod

Mod percentage is in radians multiplied by 100

ConfusionOffset#
----------------

ConfusionX
----------

ConfusionXOffset
----------------

ConfusionXOffset#
-----------------

ConfusionY
----------

ConfusionYOffset
----------------

ConfusionYOffset#
-----------------

ConfusionZ
----------

ConfusionZOffset
----------------

ConfusionZOffset#
-----------------

Converge
--------

Cosclip
-------

Cosec
-----

CouplesMirror
-------------

CouplesSwapNotes
----------------

Cover
-----

Cross
-----

Reverses the middle two columns

https://fms-cat.com/flip-invert/

CubicX
------

CubicXOffset
------------

CubicY
------

CubicYOffset
------------

CubicZ
------

CubicZOffset
------------

CustomNoteFlash
---------------

Dark
----

Hides the receptors, while keeping the blue/yellow/green/etc. flashes when tapping on notes

Dark#
-----

Diffuse|%|%|%
-------------

Digital
-------

Digitaloffset
-------------

Digitalperiod
-------------

Digitalsteps
------------

Digitalz
--------

Digitalzoffset
--------------

Digitalzperiod
--------------

Digitalzsteps
-------------

Disablemines
------------

Distant
-------

Dizzy
-----

The notes spin in their lanes

DizzyHolds
----------

Makes hold heads behave like regular tap notes with mods like Dizzy or Confusion

DrawDistance
------------

DrawDistanceBack
----------------

DrawDistanceFront
-----------------

DrawSize
--------

Determines how far back the arrows start drawing

DrawSizeBack
------------

DrawSizeFront
-------------

Drunk
-----

The notes/receptors sway left and right

DrunkOffset
-----------

DrunkPeriod
-----------

DrunkSize
---------

DrunkSpacing
------------

DrunkSpeed
----------

DrunkY
------

DrunkYOffset
------------

DrunkYPeriod
------------

DrunkYSize
----------

DrunkYSpacing
-------------

DrunkYSpeed
-----------

DrunkZ
------

DrunkZOffset
------------

DrunkZPeriod
------------

DrunkZSize
----------

DrunkZSpacing
-------------

DrunkZSpeed
-----------

Echo
----

Expand
------

ExpandPeriod
------------

ExpandSize
----------

FadeBottom|%|%|%
----------------

FadeBottomOffset|%|%|%
----------------------

FadeTop|%|%|%
-------------

FadeTopOffset|%|%|%
-------------------

FallX#|%|%|%
------------

FallX|%|%|%
-----------

FallY#|%|%|%
------------

FallY|%|%|%
-----------

FallZ#|%|%|%
------------

FallZ|%|%|%
-----------

Flip
----

Floored
-------

GayHolds
--------

The opposite of StraightHolds

GayHolds#
---------

Glitchytan
----------

GlobalModTimer
--------------

GlobalModTimerMult
------------------

GlobalModTimerOffset
--------------------

Grain
-----

Granulate
---------

Halgun
------

Hallway
-------

Hidden
------

Hiddenb
-------

Hiddenblue
----------

Hiddenblueoffset
----------------

Hiddenbo
--------

Hiddeng
-------

Hiddengo
--------

Hiddengreen
-----------

Hiddengreenoffset
-----------------

Hiddenoffset
------------

Hiddenr
-------

Hiddenred
---------

Hiddenredoffset
---------------

Hiddenro
--------

Hide
----

HideHolds
---------

Hides the hold bit of hold notes

HideHolds#
----------

HideMines
---------

Hides mines

HideMines#
----------

HideNoteFlash
-------------

Hides the blue/yellow/green/etc. receptor flashes when arrows are hit

HideNoteFlash#
--------------

HideNoteFlashes
---------------

HideNoteFlashes#
----------------

HoldCull
--------

HoldGirth
---------

HoldGirth#
----------

HoldStealth
-----------

HoldStealth#
------------

HoldStorolls
------------

HoldTiny
--------

HoldTiny#
---------

Incoming
--------

Invert
------

Judgescale
----------

Land
----

Left
----

Little
------

LongBoy / LongBoys / LongHolds
------------------------------

Makes holds appear longer/shorter than they actually are

``100% LongHolds`` makes holds appear to be double their length, ``-100% LongHolds`` hides holds entirely.

M<speed>
--------
where ``<speed>`` is a float

|since_itg|

Sets the scroll speed to use MMod at a given speed

ManualNoteFlash
---------------

Mines
-----

MineStealth
-----------

MineStealth#
------------

Mini
----
Shrinks/grows the playfield

``100% Mini`` makes the playfield half size, ``200% Mini`` makes the playfield zero sized, negative values make the
playfield larger.

Mirror
------

ModTimer
--------

ModTimerMult
------------

ModTimerOffset
--------------

Movex
-----

Movex#
------

Movey
-----

Movey#
------

Movez
-----

Movez#
------

Nofreeze
--------

Nohands
-------

Noholdjudge
-----------

Noholds
-------

Nojumps
-------

Nomines
-------

Noquads
-------

Norolls
-------

Nostretch
---------

NoteSkew
--------

NoteSkew#
---------

NoteSkewType
------------

NoteSkewX
---------

NoteSkewX#
----------

NoteSkewY
---------

NoteSkewY#
----------

NoteSkin
--------

Orient
------

|since_notitg_v4|

Additionally rotates arrows in the direction of travel relative to "upwards".

It can also be used in percentages, to increase or decrease or even invert the effect.

For downwards scroll (e.g. with Reverse or splines), combine this mod with 314% ConfusionOffset

Overhead
--------

ParabolaX
---------

ParabolaXOffset
---------------

ParabolaY
---------

ParabolaYOffset
---------------

ParabolaZ
---------

ParabolaZOffset
---------------

Passmark
--------

Planted
-------

Pulse
-----

PulseInner
----------

PulseOffset
-----------

PulseOuter
----------

PulsePeriod
-----------

Quick
-----

Random
------

Randomize
---------

RandomizeMirror
---------------

RandomizeOffset
---------------

RandomSpeed
-----------

RandomVanish
------------

ReceptorZBuffer
---------------

ReceptorZBuffer#
----------------

Reverse
-------

Pulls all four receptors to the bottom of the playfield and makes the arrows come down from the top

https://fms-cat.com/flip-invert/

Reverse#
--------

ReverseType
-----------

Right
-----

Roll
----

RotationX
---------

RotationY
---------

RotationZ
---------

Sawtooth
--------

SawtoothOffset
--------------

SawtoothPeriod
--------------

SawtoothSize
------------

SawtoothZ
---------

SawtoothZOffset
---------------

SawtoothZPeriod
---------------

SawtoothZsize
-------------

ScreenFilter
------------

ShrinkLinear
------------

ShrinkLinearX
-------------

ShrinkLinearY
-------------

ShrinkLinearZ
-------------

ShrinkMult
----------

ShrinkMultX
-----------

ShrinkMultY
-----------

ShrinkMultZ
-----------

Shuffle
-------

Sinclip
-------

SkewType
--------

SkewX
-----

SkewY
-----

Skippy
------

SmartBlender
------------

SoftShuffle
-----------

Space
-----

SpiralHolds / HoldType
----------------------

A different hold renderer (very noticable on the Y axis)

SpiralX
-------

SpiralXOffset
-------------

SpiralXPeriod
-------------

SpiralY
-------

SpiralYOffset
-------------

SpiralYPeriod
-------------

SpiralZ
-------

SpiralZOffset
-------------

SpiralZPeriod
-------------

Spline#rotx#
------------

Spline#rotxoffset#
------------------

Spline#rotxreset
----------------

Spline#roty#
------------

Spline#rotyoffset#
------------------

Spline#rotyreset
----------------

Spline#rotz#
------------

Spline#rotzoffset#
------------------

Spline#rotzreset
----------------

Spline#size#
------------

Spline#sizeoffset#
------------------

Spline#sizereset
----------------

Spline#skew#
------------

Spline#skewoffset#
------------------

Spline#skewreset
----------------

Spline#stealth#
---------------

Spline#stealthoffset#
---------------------

Spline#stealthreset
-------------------

Spline#tiny#
------------

Spline#tinyoffset#
------------------

Spline#tinyreset
----------------

Spline#x#
---------

Spline#xoffset#
---------------

Spline#xreset
-------------

Spline#y#
---------

Spline#yoffset#
---------------

Spline#yreset
-------------

Spline#z#
---------

Spline#zoffset#
---------------

Spline#zoom#
------------

Spline#zoomoffset#
------------------

Spline#zoomreset
----------------

Spline#zreset
-------------

SplineRotx#
-----------

SplineRotxoffset#
-----------------

SplineRotxreset
---------------

SplineRotxtype
--------------

SplineRoty#
-----------

SplineRotyoffset#
-----------------

SplineRotyreset
---------------

SplineRotytype
--------------

SplineRotz#
-----------

SplineRotzoffset#
-----------------

SplineRotzreset
---------------

SplineRotztype
--------------

SplineSize#
-----------

SplineSizeoffset#
-----------------

SplineSizereset
---------------

SplineSizetype
--------------

SplineSkew#
-----------

SplineSkewoffset#
-----------------

SplineSkewreset
---------------

SplineSkewtype
--------------

SplineStealth#
--------------

SplineStealthoffset#
--------------------

SplineStealthreset
------------------

SplineStealthtype
-----------------

SplineTiny#
-----------

SplineTinyoffset#
-----------------

SplineTinyreset
---------------

SplineTinytype
--------------

SplineX#
--------

SplineXoffset#
--------------

SplineXreset
------------

SplineXtype
-----------

SplineY#
--------

SplineYoffset#
--------------

SplineYreset
------------

SplineYtype
-----------

SplineZ#
--------

SplineZoffset#
--------------

SplineZoom#
-----------

SplineZoomoffset#
-----------------

SplineZoomreset
---------------

SplineZoomtype
--------------

SplineZreset
------------

SplineZtype
-----------

Split
-----

Reverses the rightmost two columns

https://fms-cat.com/flip-invert/

SpookyShuffle
-------------

Square
------

SquareOffset
------------

SquarePeriod
------------

SquareZ
-------

SquareZOffset
-------------

SquareZPeriod
-------------

Stealth
-------

|since_itg|

Flashes arrows (white by default) and hides them as they approach the receptors

Stealth#
--------

StealthB
--------

StealthBlue
-----------

StealthG
--------

StealthGB
---------

StealthGG
---------

StealthGlow#|%|%|%
------------------

StealthGlow|%|%|%
-----------------

StealthGlowBlue
---------------

StealthGlowGreen
----------------

StealthGlowRed
--------------

StealthGR
---------

StealthGreen
------------

StealthPastReceptors
--------------------

If you have Stealth enabled (or a stealth spline), by default arrows that aren't hit will appear again once they fly
past the receptors - this makes them not do that

StealthR
--------

StealthRed
----------

StealthType
-----------

Stomp
-----

StraightHolds
-------------

StraightHolds#
--------------

SubtractScore
-------------

Sudden
------

|since_itg|

Makes arrows appear on the notefield closer to the receptors than usual

SuddenB
-------

SuddenBlue
----------

SuddenBlueoffset
----------------

SuddenBO
--------

SuddenG
-------

SuddenGO
--------

SuddenGreen
-----------

SuddenGreenoffset
-----------------

SuddenOffset
------------

Toy around with different percentage values to adjust how far away arrows appear with the Sudden mod

Stealth splines are easier if you want to be exact

SuddenR
-------

SuddenRed
---------

SuddenRedOffset
---------------

SuddenRO
--------

SuperShuffle
------------

SwapLeftRight
-------------

SwapSides
---------

SwapUpDown
----------

TanBumpy
--------

TanBumpy#
---------

TanBumpyOffset
--------------

TanBumpyPeriod
--------------

TanBumpySize
------------

TanBumpyX
---------

TanBumpyX#
----------

TanBumpyXOffset
---------------

TanBumpyXPeriod
---------------

TanBumpyXSize
-------------

TanBumpyY
---------

TanBumpyY#
----------

TanBumpyYOffset
---------------

TanBumpyYPeriod
---------------

TanBumpyYSize
-------------

TanBumpyZ#
----------

TanClip
-------

TanDigital
----------

TanDigitalOffset
----------------

TanDigitalPeriod
----------------

TanDigitalSteps
---------------

TanDigitalZ
-----------

TanDigitalZOffset
-----------------

TanDigitalZPeriod
-----------------

TanDigitalZSteps
----------------

TanDrunk
--------

TanDrunkOffset
--------------

TanDrunkPeriod
--------------

TanDrunkSize
------------

TanDrunkSpacing
---------------

TanDrunkSpeed
-------------

TanDrunkY
---------

TanDrunkYOffset
---------------

TanDrunkYPeriod
---------------

TanDrunkYSize
-------------

TanDrunkYSpacing
----------------

TanDrunkYSpeed
--------------

TanDrunkZ
---------

TanDrunkZOffset
---------------

TanDrunkZPeriod
---------------

TanDrunkZSize
-------------

TanDrunkZSpacing
----------------

TanDrunkZSpeed
--------------

TanExpand
---------

TanExpandPeriod
---------------

TanExpandSize
-------------

TanPulse
--------

TanPulseInner
-------------

TanPulseOffset
--------------

TanPulseOuter
-------------

TanPulsePeriod
--------------

TanTipsy
--------

TanTipsyOffset
--------------

TanTipsySpacing
---------------

TanTipsySpeed
-------------

TanTornado
----------

TanTornadoOffset
----------------

TanTornadoPeriod
----------------

TanTornadoZ
-----------

TanTornadoZOffset
-----------------

TanTornadoZPeriod
-----------------

TextureFilterOff
----------------

TextureFilterOff#
-----------------

Timer
-----

TimerMult
---------

TimerOffset
-----------

Timing
------

Tiny
----

Tiny#
-----

TinyX
-----

TinyX#
------

TinyY
-----

TinyY#
------

TinyZ
-----

TinyZ#
------

Tipsy
-----

TipsyOffset
-----------

TipsySpacing
------------

TipsySpeed
----------

Tornado
-------

|since_itg|

The notes fly in from the left, to the right, as they move towards the receptors

TornadoOffset
-------------

TornadoPeriod
-------------

TornadoZ
--------

TornadoZOffset
--------------

TornadoZPeriod
--------------

Turn
----

Twirl
-----

The notes and their holds spin in 3D (around the up axis)

This creates a nice twisting appearance for the holds

Twister
-------

Ultraman
--------

Vanish
------

VanishOffset
------------

VanishSize
----------

Wave
----

Wave#
-----

WaveOffset
----------

WaveOffset#
-----------

WavePeriod
----------

WavePeriod#
-----------

WaveSize
--------

Wide
----

WireFrame
---------

WireFrame#
----------

WireFrameGirth
--------------

WireFrameGirth#
---------------

WireFrameWidth
--------------

WireFrameWidth#
---------------

X
-

Y
-

Z
-

ZBuffer
-------

Makes arrows/holds read from and write to the depth/Z buffer

Zigzag
------

ZigzagOffset
------------

ZigzagPeriod
------------

ZigzagSize
----------

ZigzagZ
-------

ZigzagZOffset
-------------

ZigzagZPeriod
-------------

ZigzagZSize
-----------

Zoom
----

ZoomX
-----

ZoomY
-----

ZoomZ
-----

ZTest
-----
