BitmapText
==========

|since_itg|

.. contents:: :local:

Description
-----------

A actor that renders text

API reference
-------------

.. lua:autoclass:: BitmapText
