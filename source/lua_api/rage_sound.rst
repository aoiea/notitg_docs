RageSound
=========

|since_notitg_v1|

.. contents:: :local:

Description
-----------

A sound object

API reference
-------------

.. lua:autoclass:: RageSound
