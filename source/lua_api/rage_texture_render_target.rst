RageTextureRenderTarget
=======================

|since_notitg_v1|

.. contents:: :local:

Description
-----------

A texture object created by an :lua:class:`ActorFrameTexture`

API reference
-------------

.. lua:autoclass:: RageTextureRenderTarget
