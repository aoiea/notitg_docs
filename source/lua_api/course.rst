Course
======

|since_itg|

.. contents:: :local:

Description
-----------

A course

API reference
-------------

.. lua:autoclass:: Course
