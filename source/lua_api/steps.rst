Steps
=====

|since_itg|

.. contents:: :local:

Description
-----------

Represents a chart in a song

API reference
-------------

.. lua:autoclass:: Steps
