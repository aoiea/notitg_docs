ActorScroller
=============

|since_itg|

.. contents:: :local:

Description
-----------

An ActorScroller creates a scrollable display of its child actors.

API reference
-------------

.. lua:autoclass:: ActorScroller
