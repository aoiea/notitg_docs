RageTexture
===========

|since_notitg_v1|

.. contents:: :local:

Description
-----------

A texture object

API reference
-------------

.. lua:autoclass:: RageTexture
