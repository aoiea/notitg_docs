RageFileManager
===============

|since_notitg_v3_1|

.. contents:: :local:

Description
-----------

Accessible from the global :lua:attr:`FILEMAN <global.global.FILEMAN>`

API reference
-------------

.. lua:autoclass:: RageFileManager
