DifficultyMeter
===============

|since_itg|

.. contents:: :local:

Description
-----------

A difficulty meter actor

API reference
-------------

.. lua:autoclass:: DifficultyMeter
