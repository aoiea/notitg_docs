Lua API
=======

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   actor
   actor_frame
   actor_scroller
   difficulty_meter
   fading_banner
   actor_frame_texture
   player
   actor_proxy
   bitmap_text
   help_display
   sprite
   model
   actor_sound
   screen_gameplay
   high_score
   high_score_list
   memory_card_manager
   message_manager
   rage_shader_program
   rage_sound
   rage_texture
   rage_texture_render_target
   rage_display
   rage_input
   rage_file_manager
   game
   song
   steps
   course
   trail
   radar_values
   profile
   stage_stats
   player_stage_stats
   game_state
   game_sound_manager
   screen_manager
   note_skin_manager
   prefs_manager
   profile_manager
   song_manager
   stats_manager
   theme_manager
   unlock_manager

   enumerations
   globals
