UnlockManager
=============

|since_itg|

.. contents:: :local:

Description
-----------

The unlock manager, accessible from the global :lua:attr:`UNLOCKMAN <global.global.UNLOCKMAN>`

API reference
-------------

.. lua:autoclass:: UnlockManager
