MessageManager
==============

|since_itg|

.. contents:: :local:

Description
-----------

Where custom message commands can be broadcast from

See :doc:`/message_commands` for more info.

API reference
-------------

.. lua:autoclass:: MessageManager
