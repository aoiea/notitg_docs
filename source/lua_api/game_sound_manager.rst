GameSoundManager
================

|since_itg|

.. contents:: :local:

Description
-----------

The :lua:class:`GameSoundManager` singleton, accessible from the global :lua:attr:`SOUND <global.global.SOUND>`

API reference
-------------

.. lua:autoclass:: GameSoundManager
